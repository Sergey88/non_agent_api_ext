<?php

/**
 * Перезвон клиенту при прослушивании записи менее X секунд.
 */
class CallbackAfterAction extends Action 
{
	/**
	 * Лимит при прослушивании в сек.
	 */
	public $call_time_limit = 0;

	/**
	 * Следующий статус.
	 */
	public $next_status;

	public $options;

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->next_status = isset($this->params['next_status']) ? $this->params['next_status'] : '';
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			if(isset($this->params['call_time_limit'])){
				$this->call_time_limit = $this->params['call_time_limit'];
			}
		}
	}

	public function run(){
		$this->db->where('uniqueid',$this->params['uniqueid']);
		$vlog = $this->db->getOne('asterisk.vicidial_log');
		if($this->dbg && !empty($vlog)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][vicidial_log record exists]'.PHP_EOL,FILE_APPEND);
		if(!empty($vlog) && $vlog['length_in_sec']<$this->call_time_limit && $vlog['length_in_sec']>0){
			if($this->dbg && !empty($vlog)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][update vicidial_list list]'.PHP_EOL,FILE_APPEND);
			$this->db->setTrace();
			$data = ['status'=>$this->next_status];
			$this->db->where('lead_id',$vlog['lead_id']);
			if($this->db->update('asterisk.vicidial_list',$data)){
			    if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').']'.$this->db->count . ' records were updated'.PHP_EOL,FILE_APPEND);
			}
			else{
			    if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').']update failed: ' . $this->db->getLastError().PHP_EOL,FILE_APPEND);
			}
		}
	}
}