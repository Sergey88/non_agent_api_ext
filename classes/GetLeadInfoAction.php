<?php

/**
 * Получение информации по лиду
 */
class GetLeadInfoAction extends Action 
{
	public $options;

	/**
	 * 
	 */
	public $search_type;

	public $access_fields = ['address1','address2','address3'];

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			$this->search_type = isset($this->params['search_type']) ? $this->params['search_type'] : '';
		}
	}

	public function run(){
		header("Content-type: application/json");
		if($this->search_type!=''){
			switch($this->search_type){
				case 'get_list' : $this->getList();break;
				default : exit;
			}
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][search_type parameter is required!]'.PHP_EOL,FILE_APPEND);
			echo json_encode(['status'=>'failed','message'=>'search_type parameter is required!']);exit;
		}
	}

	/**
	 * Возвращает список листов
	 */
	public function getList(){
		$inb_params = [];
		foreach($this->params as $k=>$v){
			if(in_array($k,$this->access_fields)){
				$inb_params[$k]=$v;
			}
		}
		if(!empty($inb_params)){
			foreach($inb_params as $k=>$v){
				$this->db->where($k,$v);
			}
			$rows = $this->db->getValue('asterisk.vicidial_list','list_id',null);
			if(!empty($rows)){
				echo json_encode(['status'=>'success','message'=>'Found '.count($rows).' list(s)','data'=>array_values($rows)]);
			}
			else{
				echo json_encode(['status'=>'success','message'=>'Lists not found','data'=>[]]);
			}
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][set one or all parameters from list {'.implode(', ',$this->access_fields).'}]'.PHP_EOL,FILE_APPEND);
			echo json_encode(['status'=>'failed','message'=>'set one or all parameters from list {'.implode(', ',$this->access_fields).'}']);exit;
		}
	}

}