<?php
function app_autoloader($class){
	if(file_exists(getcwd().'/classes/'.$class.'.php')){
		include 'classes/' . $class . '.php';
		if(!class_exists($class)){
			echo 'Class `'.$class.'` doesn\'t exists!';
		}
	}
	else{
		echo 'File '.$class.'.php doesn\'t exists!';
		exit;
	}
	
}

spl_autoload_register('app_autoloader');