<?php
class Action
{
	public $dbg;
	public $db;
	public $log_file;
	public $options;
	public $params;
	public $act;

	public function __construct($action,$options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			if($this->dbg) file_put_contents($this->log_file,'['.getmypid().']['.date('Y-m-d H:i:s').']'.print_r($this->params,true).PHP_EOL,FILE_APPEND);
			$this->act = $this->handleAction($action,$options);
		}
	}

	/**
	 * Фабрика экшинов.
	 */
	public function handleAction($action,$options){
		switch($action){
			case 'callback_after_few_seconds' : return new CallbackAfterAction($options);break;
			case 'move_lead' : return new MoveLeadAction($options);break;
			case 'add_unique_lead' : return new AddUniqueLeadAction($options);break;
			case 'get_lead_info' : return new GetLeadInfoAction($options);break;
			case 'add_lead_post' : return new AddLeadPostAction($options);break;
			case 'add_lead_custom' : return new AddLeadCustomAction($options);break;
			default : return;
		}
	}

	public function run(){
		if($this->db && $this->db->ping()){
			$this->act->run();
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][Failed connect to db]'.PHP_EOL,FILE_APPEND);
		}
		
	}

}