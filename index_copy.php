<?php
include 'bootstrap.php';
if($dbg) file_put_contents($log_file,'['.getmypid().']============================================'.PHP_EOL,FILE_APPEND);
if(isset($_GET['action']) && $_GET['action']!=''){
	$action = $_GET['action'];
	if($dbg) file_put_contents($log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][action:'.$action.']'.PHP_EOL,FILE_APPEND);

	$db = new MysqliDb ($db_host, $db_user, $db_pass, $db_name);
	$a = new Action($action,['db'=>$db,'dbg'=>$dbg,'log_file'=>$log_file,'params'=>$_GET]);
	$a->run();
	
}
else{
	if($dbg)
	echo 'Set `action` parameter for request!';
	exit;
}

