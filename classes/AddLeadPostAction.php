<?php

/**
 * Добавление лида POST-запросом
 */
class AddLeadPostAction extends Action 
{
	/**
	 * Номер телефона лида. Формат 10 цифр
	 */
	public $phone_number;

	public $phone_length = 10;

	public $inn_length = 10;

	public $request_type;

	public $func;

	public $ignore_new = 1;// игнорировать статус NEW, все равно делать вставку

	private $_response;

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->phone_number = isset($this->params['phone_number']) ? $this->params['phone_number'] : '';
			$this->request_type = isset($this->params['request_type']) ? $this->params['request_type'] : '';
			$this->func = isset($this->params['function']) ? $this->params['function'] : 'add_lead';
			$this->ignore_new = isset($this->params['ignore_new']) ? $this->params['ignore_new'] : 1;
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			$this->_response = new Response;
		}
	}

	public function run(){
		if($this->request_type==''){
			$this->defaultAct();
		}
		elseif($this->request_type=='unex_custom'){
			$this->unexCustom();
		}
		else{
			$err_msg = 'Set request_type for adding lead!';
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').']['.$err_msg.']'.PHP_EOL,FILE_APPEND);
			$this->response()->setStatus(Response::FAILED)->setMessage($err_msg)->result();
		}
	}

	public function defaultAct(){
		$query_string = 'function='.$this->func.'&';
		foreach($this->params as $prm=>$val){
			if($prm=='phone_number'){
				$phone_number = $val;
			}		
			if($prm=='list_id'){
				$list_id = $val;
			}
			if($prm=='vendor_lead_code'){
				$vendor_lead_code = $val;
			}	
			if(!in_array($prm,['action','request_type']))
				$query_string .= $prm.'='.urlencode($val).'&';
		}
		$query_string = rtrim($query_string,"&");
		$to_do_insert = true;
		if(!$this->ignore_new && isset($phone_number) && isset($list_id) && isset($vendor_lead_code)){
			$this->db->where('list_id',$list_id);
			$this->db->where('vendor_lead_code',$vendor_lead_code);
			$this->db->where('phone_number',$phone_number);
			$this->db->where('status',['NEW','NA','N','AB','B','A','ADC','DROP','SVYCLM'],'IN');
			$row = $this->db->getOne('asterisk.vicidial_list');
			if(isset($row['lead_id'])){
				$this->db->where('lead_id',$row['lead_id']);
				$data = [
					'user' => 'apiuser',
					'called_since_last_reset' => 'N',
					'called_count' => '0',
					'rank' => '100',
					'status' => 'NEW',
				];
				$this->db->update('asterisk.vicidial_list',$data);
			}
			//$ignore_row = $this->db->getOne('asterisk.vicidial_list');
			$to_do_insert = isset($row['lead_id']) ? 0 : 1;
		}
		if($to_do_insert){
			$endpoint = 'http://'.$_SERVER['HTTP_HOST'].'/vicidial/non_agent_api.php?'.$query_string;
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...][endpoint:'.$endpoint.']'.PHP_EOL,FILE_APPEND);
			$content = Helper::request($endpoint);
			$resp_arr = explode('|',$content);
			if(isset($resp_arr[0]) && strpos(strtolower($resp_arr[0]),'success: add_lead lead has been added')!==false && isset($resp_arr[2])){
				if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...]'.PHP_EOL,FILE_APPEND);
				$this->response()->setData(['lead_id'=>$resp_arr[2]])->setMessage($resp_arr[0])->result();
			}
			else{
				$this->response()->setStatus(Response::FAILED)->setMessage($resp_arr[0])->result();
			}
		}
		else{
			if(isset($row['lead_id'])){
				$this->response()->setStatus(Response::SUCCESS)->setMessage('Lead has been updated('.$row['lead_id'].').')->result();
			}
			else{
				$this->response()->setStatus(Response::FAILED)->setMessage('Lead exists with status NEW.')->result();
			}
		}
	}

	/*
	 * HANDLER FOR UNEX credit_request_id
	 */
	public function unexCustom(){
		$query_string = 'function='.$this->func.'&';
		foreach($this->params as $prm=>$val){
			if(!in_array($prm,['action','request_type']))
				$query_string .= $prm.'='.urlencode($val).'&';
		}
		$query_string = rtrim($query_string,"&");
		$endpoint = 'http://'.$_SERVER['HTTP_HOST'].'/vicidial/non_agent_api.php?'.$query_string;
		if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...][endpoint:'.$endpoint.']'.PHP_EOL,FILE_APPEND);
		$content = Helper::request($endpoint);
		$resp_arr = explode('|',$content);
		if(isset($resp_arr[0]) && strpos(strtolower($resp_arr[0]),'success: add_lead lead has been added')!==false && isset($resp_arr[2])){
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...]'.PHP_EOL,FILE_APPEND);
			$this->response()->setData(['lead_id'=>$resp_arr[2]])->setMessage($resp_arr[0])->result();
		}
		else{
			$this->response()->setStatus(Response::FAILED)->setMessage($resp_arr[0])->result();
		}
	}

	public function validPhone($phone){
		return strlen($phone) == $this->phone_length;
	}

	public function response(){
		return $this->_response;
	}

}