<?php

class Helper 
{
	public static function addMinutes($min){
		$time = new DateTime(date('Y-m-d H:i:s'));
		$time->add(new DateInterval('PT' . $min . 'M'));
		return $time->format('Y-m-d H:i:s');
	}

	public static function excludeQuotes($str){
		return strtr($str,['"'=>'\"',"'"=>"\'"]);
	}

	public static function clearStr($str){
		return trim(strip_tags(self::excludeQuotes($str)));
	}

	public static function request($endpoint){
        $curl = curl_init(); 
        if (!$curl) {
            die("Couldn't initialize a cURL handle"); 
        }
        curl_setopt($curl, CURLOPT_URL, $endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($curl); 
        if (curl_errno($curl))         {
            echo 'cURL error: ' . curl_error($curl); 
        } 
        else { 
            // cURL executed successfully
            //print_r(curl_getinfo($curl)); 
            return $html;
        }
        curl_close($curl);
        return;
	}

	/**
	 * Форматирует номер телефона к 10-ти цифрам
	 * @param string $phone_number
	 * @param array $formats - с каких символов может начинаться номер
	 * @return string $phone_number
	 */
	/*public static function formatPhone($phone_number,$formats = []){
		if($phone_number!='' && strlen($phone_number>=10 && !empty($formats))){
			foreach($formats as $f){
				echo $f.PHP_EOL;
				preg_match('/^('.$f.')/', $phone_number, $matches, PREG_OFFSET_CAPTURE);
				print_r($matches);
			}
		}
		echo PHP_EOL.$phone_number;
		return $phone_number;
	}*/
}