<?php
header('Content-Type: text/txt; charset=utf-8');
?>
v.1.3.8
Дополнительный API.
Вызывается постердством GET-запроса.
Для работы нужен только каталог /srv/www/htdocs/non_agent_api_ext
Логи пишутся, только если в файле /srv/www/htdocs/non_agent_api_ext/bootstrap.php переменная $dbg = 1.
В этом же файле можно указать каталог для логирования и параметры подключения к БД.

Общий пример:
http://server/non_agent_api_ext/index.php?action=some_action&param1=value1&param2=value2
Также можно прописывать стандартные параметры по типу: uniqueid=--A--uniqueid--B--&phone_number=--A--phone_number--B--&lead_id=--A--lead_id--B--


Доспупные действия:

	add_unique_lead - Добавление лида, исключая дубли и указанные статусы
	add_lead_post - Добавление лида POST-запросом
	add_lead_custom - Добавление лида с кастомной логикой
	callback_after_few_seconds - Перезвон клиенту при прослушивании записи менее X секунд.
	move_lead - Переместить лид/скопировать лид  в другой список – если получили статус в базововм списке.
	get_lead_info - Поиск определенной информации лида


===========================================================================================================
ACTION: add_unique_lead
===========================================================================================================
Расширение для стандартной функции add_lead.
REQUIRED:
	`phone_number` - Номер телефона (12 знаков)
	`list_id` - Список, в который добавляется лид
OPTIONS:
	`exclude_statuses` - Если находим в системе дубль по номеру телефона, то исключаем указанные статусы. Статусы указываются через запятую. По умолчанию исключаются статусы 'NEW,NA,INCALL'

EXAMPLES:
	http://server/non_agent_api_ext/index.php?source=test&user=apiuser&pass=apiuser&function=add_lead&phone_number=380638256536&list_id=1111&dnc_check=N&first_name=Sergey&last_name=Rospopov&custom_fields=Y&order_code=E123543&profile_phone=380638256536&order_phone=380638256536&order_date=25.03.2019%2014:02:23&action=add_unique_lead&exclude_statuses=NEW,NA,INCALL


===========================================================================================================
ACTION: add_lead_post
===========================================================================================================
Расширение для стандартной функции add_lead посредством POST-запроса.
REQUIRED:
	`phone_number` - Номер телефона
	`list_id` - Список, в который добавляется лид
OPTIONS:
	`request_type` - кастомная реализация вставки лида.
	`ignore_new` - запретить вставку лида(0), если существует запись в таком же листе, с таким же номером и со статусом NEW.

EXAMPLES:
	http://server/non_agent_api_ext/index.php
	[
		'action' => 'add_lead_post',
		'list_id' => '888',
		'phone_number' => '0638256530',
		'first_name' => 'test',
		'vendor_lead_code' => '98372',
		'function' => 'add_lead',
		'user' => 'user',
		'pass' => 'password',
	]


===========================================================================================================
ACTION: add_lead_custom
===========================================================================================================
Расширение для стандартной функции add_lead посредством с кастомной логикой.
REQUIRED:
	`phone_number` - Номер телефона
	`list_id` - Список, в который добавляется лид
	`request_type` - Название метода обработки. В зависимости от значения, могут быть дополительные get-параметры.

EXAMPLES:
	http://server/non_agent_api_ext/index.php?action=add_lead_custom&request_type=unex_to_kvadra&rand_koef=60&external_list_id=1110&first_name=Иванов Иван Иванович&last_name=335110539188888888


===========================================================================================================
ACTION: callback_after_few_seconds
===========================================================================================================
REQUIRED:
	`uniqueid` - id звонка.
	`call_time_limit` - Нужно указывать в секудах. Если разговор длился меньше этого времени, то проставляем статус `next_status`
	`next_status` - статус, на который переводим лид.

EXAMPLES:
	http://server/non_agent_api_ext/index.php?action=callback_after_few_seconds&call_time_limit=5&next_status=5sec&uniqueid=--A--uniqueid--B--&phone_number=--A--phone_number--B--&lead_id=--A--lead_id--B--



===========================================================================================================
ACTION: move_lead
===========================================================================================================
REQUIRED:
	`operation_type` - тип операции с лидом (1. copy - копирование лида в список `dest_list`; 2. move - переместить лид в список `dest_list` ).
	`dest_list` - список
	`lead_id` 

OPTIONS:
	`include_statuses` - статусы, при которых учитывается перемещение/копирование
	`called_since_last_reset` - сброс лида (Y,N)

EXAMPLES:
Копируем лид 954158 в список 889:
http://server/non_agent_api_ext/index.php?action=move_lead&lead_id=954158&dest_list=889&operation_type=copy
Перемещаем лид 954158 в список 888:
http://server/non_agent_api_ext/index.php?action=move_lead&lead_id=954158&dest_list=888&operation_type=move
Перемещаем лид 954158 в список 888 с учетом статусов SVYCLM,NEW:
http://server/non_agent_api_ext/index.php?action=move_lead&lead_id=954158&dest_list=888&operation_type=move&include_statuses=SVYCLM,NEW


===========================================================================================================
ACTION: get_lead_info
===========================================================================================================
REQUIRED:
	`search_type` - возможные значения: 'get_list' - поиск листов по полям (address1,address2,address3).

EXAMPLES:
Поиск по одному полю:
http://server/non_agent_api_ext/index.php?action=get_lead_info&search_type=get_list&address1=3155416930
Поиск по двум полям:
http://server/non_agent_api_ext/index.php?action=get_lead_info&search_type=get_list&address1=3155416930&address3=1234