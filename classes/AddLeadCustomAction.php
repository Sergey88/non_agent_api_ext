<?php

/**
 * Добавление лида с кастомной обработкой
 */
class AddLeadCustomAction extends Action 
{
	public $options;

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->list_id = isset($this->params['list_id']) ? $this->params['list_id'] : '';
			$this->phone_number = isset($this->params['phone_number']) ? $this->params['phone_number'] : '';
			$this->request_type = isset($this->params['request_type']) ? $this->params['request_type'] : '';
			$this->rand_koef = isset($this->params['rand_koef']) ? $this->params['rand_koef'] : 50;
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
		}
	}

	public function run(){
		if($this->request_type=='unex_to_kvadra'){
			$this->addToKvadra();
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][set request_type!]'.PHP_EOL,FILE_APPEND);
		}
	}

	public function addToKvadra(){		
		$rand = rand(1,100);
		if($rand<=$this->rand_koef){
			//ADD LEAD TO QUADRA
			$kv_endpoint = 'https://db-kvadra.proasterisk.pro/vicidial/non_agent_api.php?source=UNEX&user=122&pass=HccBjUgRr0nenlp&function=update_lead&search_location=LIST&search_method=PHONE_NUMBER&insert_if_not_found=Y&reset_lead=Y&status=NEW&phone_code=380&vendor_lead_code=UNEX&add_to_hopper=Y&hopper_priority=0&custom_fields=Y';
			$phone_number = (strlen($_GET['phone_number'])==10 && substr($_GET['phone_number'],0,1)=='0') ? substr($_GET['phone_number'],1) : $_GET['phone_number'];
			$kv_endpoint .= '&phone_number='.$phone_number;
			$kv_endpoint .= '&list_id='.(isset($_GET['external_list_id']) ? self::format($_GET['external_list_id']) : '');
			$kv_endpoint .= '&first_name='.(isset($_GET['first_name']) ? self::format($_GET['first_name']) : '');
			$kv_endpoint .= '&last_name='.(isset($_GET['last_name']) ? self::format($_GET['last_name']) : '');
			$kv_endpoint .= '&date_of_birth='.(isset($_GET['date_of_birth']) ? self::format($_GET['date_of_birth']) : '');
			$kv_endpoint .= '&state='.(isset($_GET['state']) ? self::format($_GET['state']) : '');
			$kv_endpoint .= '&province='.(isset($_GET['province']) ? self::format($_GET['province']) : '');
			$kv_endpoint .= '&address3='.(isset($_GET['address3']) ? self::format($_GET['address3']) : '');
			$kv_endpoint .= '&city='.(isset($_GET['city']) ? self::format($_GET['city']) : '');
			$kv_endpoint .= '&address1='.(isset($_GET['address1']) ? self::format($_GET['address1']) : '');
			$kv_endpoint .= '&address2='.(isset($_GET['address2']) ? self::format($_GET['address2']) : '');
			$kv_endpoint .= '&postal_code='.(isset($_GET['postal_code']) ? self::format($_GET['postal_code']) : '');
			$kv_endpoint .= '&title='.(isset($_GET['title']) ? self::format($_GET['title']) : '');
			$kv_endpoint .= '&source_id='.(isset($_GET['source_id']) ? self::format($_GET['source_id']) : '');
			$kv_endpoint .= '&comments='.(isset($_GET['comments']) ? self::format($_GET['comments']) : '');
			$kv_endpoint .= '&security_phrase='.(isset($_GET['security_phrase']) ? self::format($_GET['security_phrase']) : '');
			$kv_endpoint .= '&unex_product='.(isset($_GET['unex_product']) ? self::format($_GET['unex_product']) : '');
			$kv_endpoint .= '&PARTNER_NAME='.(isset($_GET['PARTNER_NAME']) ? self::format($_GET['PARTNER_NAME']) : '');
			$kv_endpoint .= '&comment_user='.(isset($_GET['comment_user']) ? self::format($_GET['comment_user']) : '');
			$kv_endpoint .= '&prosrochka='.(isset($_GET['prosrochka']) ? self::format($_GET['prosrochka']) : '');
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...][endpoint:'.$kv_endpoint.']'.PHP_EOL,FILE_APPEND);
			$content = Helper::request($kv_endpoint);
			echo 'ADD TO KVADRA:'.PHP_EOL.$content;
			$data = [
				'phone_number'=>$phone_number,
				'external_list_id'=>$_GET['external_list_id'],
				'phone_number'=>$phone_number,
				'endpoint'=>$kv_endpoint,
				'response'=>$content,
			];
			$this->db->insert('asterisk.kvadra_leads',$data);
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][ADD TO KVADRA:'.$content.']'.PHP_EOL,FILE_APPEND);
		}
		else{
			//ADD LEAD TO UNEX
			$un_endpoint = 'http://'.$_SERVER['HTTP_HOST'].'/vicidial/non_agent_api.php?'.$_SERVER['QUERY_STRING'];
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...][endpoint:'.$endpoint.']'.PHP_EOL,FILE_APPEND);
			$content = Helper::request($un_endpoint);
			echo 'ADD TO UNEX:'.PHP_EOL.$content;
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][ADD TO UNEX:'.$content.']'.PHP_EOL,FILE_APPEND);
		}
	}

	public static function format($str){
		return urlencode($str);
	}
}