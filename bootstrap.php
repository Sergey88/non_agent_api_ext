<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
require_once 'vendor/PHP-MySQLi-Database-Class-master/MysqliDb.php';
include 'autoload.php';

if ( file_exists("/etc/astguiclient.conf") )
	{
	$DBCagc = file("/etc/astguiclient.conf");
	foreach ($DBCagc as $DBCline) 
		{
		$DBCline = preg_replace("/ |>|\n|\r|\t|\#.*|;.*/","",$DBCline);
		if (preg_match("/^PATHlogs/", $DBCline))
			{$PATHlogs = $DBCline;   $PATHlogs = preg_replace("/.*=/","",$PATHlogs);}
		if (preg_match("/^PATHweb/", $DBCline))
			{$WeBServeRRooT = $DBCline;   $WeBServeRRooT = preg_replace("/.*=/","",$WeBServeRRooT);}
		if (preg_match("/^VARserver_ip/", $DBCline))
			{$WEBserver_ip = $DBCline;   $WEBserver_ip = preg_replace("/.*=/","",$WEBserver_ip);}
		if (preg_match("/^VARDB_server/", $DBCline))
			{$VARDB_server = $DBCline;   $VARDB_server = preg_replace("/.*=/","",$VARDB_server);}
		if (preg_match("/^VARDB_database/", $DBCline))
			{$VARDB_database = $DBCline;   $VARDB_database = preg_replace("/.*=/","",$VARDB_database);}
		if (preg_match("/^VARDB_user/", $DBCline))
			{$VARDB_user = $DBCline;   $VARDB_user = preg_replace("/.*=/","",$VARDB_user);}
		if (preg_match("/^VARDB_pass/", $DBCline))
			{$VARDB_pass = $DBCline;   $VARDB_pass = preg_replace("/.*=/","",$VARDB_pass);}
		if (preg_match("/^VARDB_port/", $DBCline))
			{$VARDB_port = $DBCline;   $VARDB_port = preg_replace("/.*=/","",$VARDB_port);}
		}
	}
else
	{
	#defaults for DB connection
	$VARDB_server = 'localhost';
	$VARDB_port = '3306';
	$VARDB_user = 'cron';
	$VARDB_pass = '1234';
	$VARDB_database = '1234';
	$WeBServeRRooT = '/usr/local/apache2/htdocs';
	}

#Вкл./Выкл. логирование (1/0)
	$dbg = 1;

#Каталог и файлы для логирования
	$log_dir = '/var/log/non_agent_api_ext';
	if (!file_exists($log_dir)) {
	    mkdir($log_dir, 0777, true);
	}
	$log_file = $log_dir.'/'.date('Y-m-d').'.txt';

#Переменные для подключения к БД
	//asterisk
	$db_host = $VARDB_server;
	$db_name = 'asterisk';
	$db_user = $VARDB_user;
	$db_pass = $VARDB_pass;
	//ccdb
	$ccdb_host = $VARDB_server;
	$ccdb_name = 'ccdb';
	$ccdb_user = $VARDB_user;
	$ccdb_pass = $VARDB_pass;
