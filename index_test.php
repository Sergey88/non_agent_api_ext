<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'bootstrap.php';
if($dbg) file_put_contents($log_file,'['.getmypid().']============================================'.PHP_EOL,FILE_APPEND);
if($_SERVER['REQUEST_METHOD']=='GET'){
	if(isset($_GET['action']) && $_GET['action']!=''){
		$action = $_GET['action'];
		if($dbg) file_put_contents($log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][action:'.$action.']'.PHP_EOL,FILE_APPEND);

		$db = new MysqliDb ($db_host, $db_user, $db_pass, $db_name);
		$a = new Action($action,['db'=>$db,'dbg'=>$dbg,'log_file'=>$log_file,'params'=>$_GET]);
		$a->run();
		
	}
	else{
		if($dbg)
		echo 'Set `action` parameter for request!';
		exit;
	}
}
elseif($_SERVER['REQUEST_METHOD']=='POST'){
	header('Content-type: application/json');
	$data = json_decode(file_get_contents("php://input"),true);
	if(isset($data['action']) && $data['action']!=''){
		$action = $data['action'];
		if($dbg) file_put_contents($log_file,'['.getmypid().']['.date('Y-m-d H:i:s').'][action:'.$action.']'.PHP_EOL,FILE_APPEND);

		$db = new MysqliDb ($db_host, $db_user, $db_pass, $db_name);
		$a = new Action($action,['db'=>$db,'dbg'=>$dbg,'log_file'=>$log_file,'params'=>$data]);
		$a->run();
	}
	else{
		if($dbg)
		echo json_encode(['message'=>'Set `action` parameter for request!']);
		exit;
	}
}


