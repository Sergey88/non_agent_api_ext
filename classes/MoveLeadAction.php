<?php

/**
 * Переместить лид/скопировать лид  в другой список – если получили статус в базововм списке.
 */
class MoveLeadAction extends Action 
{
	/**
	 * Тип работы. Переместить в список лид или скопировать в список.
	 */
	public $operation_type;

	/**
	 * Новый список для лида.
	 */
	public $dest_list;

	/**
	 * Статусы, при которых учитывается перемещение/копирование
	 */
	public $include_statuses;

	public $called_since_last_reset;

	public $lead_id;

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			$this->operation_type = isset($this->params['operation_type']) ? $this->params['operation_type'] : '';
			$this->dest_list = isset($this->params['dest_list']) ? $this->params['dest_list'] : '';
			$this->lead_id = isset($this->params['lead_id']) ? $this->params['lead_id'] : '';
			$this->include_statuses = isset($this->params['include_statuses']) ? $this->params['include_statuses'] : '';
			$this->called_since_last_reset = isset($this->params['called_since_last_reset']) ? $this->params['called_since_last_reset'] : '';
		}
	}

	public function run(){
		$lead_id = $this->lead_id;
		if($this->lead_id!=''){
			$operation_type = $this->operation_type;
			if($operation_type!=''){
				$dest_list = $this->dest_list;
				if($dest_list!=''){
					$this->db->where('lead_id',$lead_id);
					$vlist = $this->db->getOne('asterisk.vicidial_list');
					if($this->dbg && !empty($vlist)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][vicidial_list record exists]'.PHP_EOL,FILE_APPEND);
					
					if(!empty($vlist)){
						if($operation_type=='move'){
							if($this->include_statuses!=''){
								$this->db->where('status',explode(',',$this->include_statuses),'IN');
							}
							$update_data = ['list_id'=>$dest_list];
							if($this->called_since_last_reset!=''){
								$update_data['called_since_last_reset'] = $this->called_since_last_reset;
							}
							$this->db->where('lead_id',$lead_id);
							$this->db->update('asterisk.vicidial_list',$update_data);
							if($this->db->getLastErrno() === 0){
							    if($this->dbg && !empty($vlist)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][lead moved to list '.$dest_list.']'.PHP_EOL,FILE_APPEND);
							}
							else{
								if($this->dbg && !empty($vlist)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][update failed:'.$this->db->getLastError().']'.PHP_EOL,FILE_APPEND);
							}
						}
						if($operation_type=='copy'){
							if( $this->include_statuses!='' && in_array($vlist['status'],explode(',',$this->include_statuses)) ){
								$data = $vlist;
								unset($data['lead_id'],$data['last_local_call_time']);
								$data['list_id'] = $dest_list;
								$data['entry_date'] = date('Y-m-d H:i:s');
								$data['modify_date'] = date('Y-m-d H:i:s');
								if($this->called_since_last_reset!=''){
									$data['called_since_last_reset'] = $this->called_since_last_reset;
								}
								$id = $this->db->insert('asterisk.vicidial_list',$data);
								if($id){
								    if($this->dbg && !empty($vlist)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][copy success to list '.$dest_list.' with lead_id '.$id.']'.PHP_EOL,FILE_APPEND);
								}
								else{
									if($this->dbg && !empty($vlist)) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][copy failed:'.$this->db->getLastError().']'.PHP_EOL,FILE_APPEND);
								}	
							}
						}
					}
					else{
						if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][vicidial_list record is empty]'.PHP_EOL,FILE_APPEND);
					}

				}
				else{
					if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][dest_list is empty]'.PHP_EOL,FILE_APPEND);
				}
			}
			else{
				if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][operation_type is empty]'.PHP_EOL,FILE_APPEND);
			}
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][lead_id is empty]'.PHP_EOL,FILE_APPEND);
		}
	}
}