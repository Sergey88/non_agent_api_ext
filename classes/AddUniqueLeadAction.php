<?php

/**
 * Добавление лида, исключая дубли и статусы $exclude_statuses
 */
class AddUniqueLeadAction extends Action 
{
	/**
	 * Список, в который добавляется лид
	 */
	public $list_id;

	public $include_lists = '';

	/**
	 * Номер телефона лида. Формат 12 цифр
	 */
	public $phone_number;

	public $vendor_lead_code;

	/**
	 * Если находим в системе дубль по номеру телефона, то исключаем следующие статусы
	 */
	public $exclude_statuses = 'NEW,NA,INCALL';

	/**
	 * Очистить phone_code
	 */
	public $reset_phone_code = 'N';

	public $gmt_offset_now;

	public $options;

	public function __construct($options){
		if(is_array($options) && !empty($options)){
			$this->options = $options;
			$this->params = isset($options['params']) ? $options['params'] : [];
			$this->dbg = isset($options['dbg']) ? $options['dbg'] : 0;
			$this->db = isset($options['db']) ? $options['db'] : false;
			$this->list_id = isset($this->params['list_id']) ? $this->params['list_id'] : '';
			$this->include_lists = isset($this->params['include_lists']) ? $this->params['include_lists'] : '';
			$this->phone_number = isset($this->params['phone_number']) ? $this->params['phone_number'] : '';
			$this->vendor_lead_code = isset($this->params['vendor_lead_code']) ? $this->params['vendor_lead_code'] : '';
			$this->gmt_offset_now = isset($this->params['gmt_offset_now']) ? $this->params['gmt_offset_now'] : '';
			$this->log_file = isset($options['log_file']) ? $options['log_file'] : '';
			if(isset($this->params['exclude_statuses']) && $this->params['exclude_statuses']!=''){
				$this->exclude_statuses = $this->params['exclude_statuses'];
			}
			if(isset($this->params['reset_phone_code']) && $this->params['reset_phone_code']!=''){
				$this->reset_phone_code = $this->params['reset_phone_code'];
			}
		}
	}

	public function run(){
		if($this->phone_number!='' && $this->list_id!='' && $this->exclude_statuses!=''){
			if(strlen($this->phone_number)!=12){
				if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][not valid phone_number:'.$this->phone_number.']'.PHP_EOL,FILE_APPEND);
				exit;
			}
			if($this->gmt_offset_now==''){
				//SET GMT OFFSET
				$today = new DateTime();
				$spring = new DateTime('March 20');
				$summer = new DateTime('June 20');
				$fall = new DateTime('September 22');
				$winter = new DateTime('December 21');
				switch(true) {
				    case $today >= $spring && $today < $summer:
				        $this->gmt_offset_now = '3,00';
				        break;
				    case $today >= $summer && $today < $fall:
				        $this->gmt_offset_now = '3,00';
				        break;
				    case $today >= $fall && $today < $winter:
				        $this->gmt_offset_now = '2,00';
				        break;
				    default:
				        $this->gmt_offset_now = '2,00';
				}
			}

			$this->db->where('phone_number','%'.$this->phone_number.'%','like');
			if($this->vendor_lead_code!=''){
				$this->db->where('vendor_lead_code',$this->vendor_lead_code);
			}
			if($this->include_lists!=''){
				$lists = explode(',',$this->include_lists);
				$lists[] = $this->list_id;
				$this->db->where('list_id',$lists,'in');
			}
			else{
				$this->db->where('list_id',$this->list_id);
			}
			$vlist = $this->db->get('asterisk.vicidial_list');
			if($vlist){
				$leads_arr = [];
				foreach($vlist as $v){
					$leads_arr[] = "'".$v['lead_id']."'";
				}
				$leads = implode(',',$leads_arr);
				if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][leads exists:'.$leads.']'.PHP_EOL,FILE_APPEND);
				//CHECK ON MAIN STATUSES
				$statuses_exists = $this->checkMainStatuses($leads);
				if($statuses_exists){
					echo json_encode(['message'=>'leads exists:'.$leads]);
				}
				else{
					//ADDITIONAL CHECK
					$is_exists = $this->additionalCheck($leads);
					if($is_exists){
						echo json_encode(['message'=>'leads exists:'.$leads]);
					}
					else{
						$this->addNewLead();
					}
				}
			}
			else{
				$this->addNewLead();
			}
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][phone_number or list_id or exclude_statuses is empty][phone:'.$this->phone_number.';list_id:'.$this->list_id.';exclude_statuses:'.$this->exclude_statuses.']'.PHP_EOL,FILE_APPEND);
		}
	}

	//ADD LEAD WITH STATUS 'NEW'
	public function addNewLead(){
		$endpoint = 'http://'.$_SERVER['HTTP_HOST'].'/vicidial/non_agent_api.php?'.$_SERVER['QUERY_STRING'];
		if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][curl request...][endpoint:'.$endpoint.']'.PHP_EOL,FILE_APPEND);
		$content = Helper::request($endpoint);
		$resp_arr = explode('|',$content);
		if(isset($resp_arr[0]) && strpos(strtolower($resp_arr[0]),'success: add_lead lead has been added')!==false && isset($resp_arr[2]) && is_numeric($resp_arr[2])){
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][lead '.$resp_arr[2].' added]'.PHP_EOL,FILE_APPEND);
			echo json_encode(['message'=>$resp_arr[0]]);
			$data['gmt_offset_now'] = number_format($this->gmt_offset_now, 2, ',', '');
			$this->db->where('lead_id',$resp_arr[2]);
			$this->db->where('list_id',$this->list_id);
			$this->db->update('asterisk.vicidial_list',$data);
			//UPDATE LEAD PHONE CODE
			$data = [];
			if($this->reset_phone_code=='Y'){
				$data['phone_code'] = '';
				$this->db->where('lead_id',$resp_arr[2]);
				$this->db->where('list_id',$this->list_id);
				$this->db->update('asterisk.vicidial_list',$data);
				if ($this->db->getLastErrno() === 0){
				    if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][lead '.$resp_arr[2].' updated]'.PHP_EOL,FILE_APPEND);
				}
				else{
					if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][update failed]['.$this->db->getLastError().']'.PHP_EOL,FILE_APPEND);
				}
			}
		}
		else{
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][lead not added]'.PHP_EOL,FILE_APPEND);
			echo json_encode(['message'=>$content]);
		}
	}

	public function additionalCheck($leads){
		$hopper_lead_exists = $this->checkInHopper($leads);
		if($hopper_lead_exists){
			if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][hopper lead exists]'.PHP_EOL,FILE_APPEND);
			return true;
		}
		else{
			$recycle_lead_exists = $this->checkRecycle($leads);
			if($recycle_lead_exists){
				if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][recycle lead exists]'.PHP_EOL,FILE_APPEND);
				return true;
			}
			else{
				$callback_lead_exists = $this->checkCallback($leads);
				if($callback_lead_exists){
					if($this->dbg) file_put_contents($this->log_file,'['.date('Y-m-d H:i:s').'][callback lead exists]'.PHP_EOL,FILE_APPEND);
					return true;
				}
			}
		}
		return false;
	}

	public function checkMainStatuses($leads){
		$this->db->where('status',explode(',',$this->exclude_statuses),'in');
		$this->db->where('lead_id',explode(',',strtr($leads,["'"=>''])),'in');
		$statuses_rec = $this->db->getOne('asterisk.vicidial_list');
		return !empty($statuses_rec) ? true : false;
	}

	//Проверка в Hopper
	public function checkInHopper($leads){
		$hopper_recs = $this->db->rawQuery("SELECT l.lead_id FROM vicidial_hopper h
			JOIN vicidial_list l on h.lead_id = l.lead_id 
			WHERE l.lead_id IN (".$leads.") LIMIT 1");
		return !empty($hopper_recs) ? true : false;
	}

	//Проверка на recycle
	public function checkRecycle($leads){
		$recycle_recs = $this->db->rawQuery("SELECT l.lead_id,l.* FROM vicidial_list l
			JOIN vicidial_lists ll ON l.list_id = ll.list_id 
			JOIN vicidial_lead_recycle r ON  r.campaign_id = ll.campaign_id AND r.active = 'Y' AND r.status =l.status AND called_since_last_reset < concat('Y',r.attempt_maximum)
			WHERE l.lead_id IN (".$leads.") LIMIT 1");
		return !empty($recycle_recs) ? true : false;
	}

	//Проверка на callback
	public function checkCallback($leads){
		$callback_recs = $this->db->rawQuery("SELECT l.* FROM vicidial_list l
			JOIN vicidial_lists ll ON l.list_id = ll.list_id 
			JOIN vicidial_campaigns c ON ll.campaign_id = c.campaign_id
			JOIN vicidial_callbacks cb ON l.lead_id = cb.lead_id AND l.status IN ('CBHOLD', 'CALLBK') AND ((cb.status = 'ACTIVE' ) OR (cb.status = 'LIVE'  ))
			WHERE l.lead_id IN (".$leads.") LIMIT 1");
		return !empty($callback_recs) ? true : false;
	}

}