<?php
$url = 'http://172.18.255.2/non_agent_api_ext/index.php';
$data = array(
    'action' => 'add_lead_post',
    'list_id' => '450',
    //fields from unex
    'phone_number' => '0638256536',
    //inn field changed to address1
    'address1' => '3231122333',
    //'first_name' => '3231122333',
    'vendor_lead_code' => '98372',
    'phone_code' => '38',
    //standart fields
    'source' => 'unex',
    'user' => 'apiuser',
    'pass' => 'apiuser',

    //...
    //'request_type' => 'unex_custom',
    //'function' => 'add_lead',
);
 
$payload = json_encode($data);
 
// Prepare new cURL resource
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
 
// Set HTTP Header for POST request 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($payload),
));
 
// Submit the POST request
$result = curl_exec($ch);
 
// Close cURL session handle
curl_close($ch);

print_r($result);