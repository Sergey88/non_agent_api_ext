<?php

class Response {

	const SUCCESS = 'success';
	const FAILED = 'failed';

	private $_status;
	private $_message;
	private $_data;

	public function __construct(){
		$this->setDefault();
	}

	public function setStatus($status = self::SUCCESS){
		$this->_status = $status;
		return $this;
	}

	public function setMessage($message = ''){
		$this->_message = $message;
		return $this;
	}

	public function setData($data = []){
		$this->_data = $data;
		return $this;
	}

	protected function setDefault(){
		return $this->setStatus()->setMessage()->setData();
	}

	public function getStatus(){
		return $this->_status;
	}

	public function getMessage(){
		return $this->_message;
	}

	public function getData(){
		return $this->_data;
	}

	public function build(){
		return [
			'status' => $this->getStatus(),
			'message' => $this->getMessage(),
			'data' => $this->getData(),
		];
	}

	public function result(){
		header('Content-type: application/json;');
		echo json_encode($this->build());
	}

}